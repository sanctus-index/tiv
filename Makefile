PGNAME ?= tiv
BINDIR ?= /usr/bin
MKCMD  ?= ./make_command.sh

default: $(PGNAME)

$(PGNAME):
	$(MKCMD) $(PGNAME)

.PHONY: clean
clean:
	rm -f -- $(PGNAME)

.PHONY: install
install:
	cp -- $(PGNAME) $(BINDIR)/$(PGNAME)

.PHONY: uninstall
uninstall:
	rm -r -- $(BINDIR)/$(PGNAME)
