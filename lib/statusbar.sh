#!/usr/bin/env bash

function statusbar {
    [[ $1 == -a ]] && {
        declare -i a=$2
        declare -i max=$3
        shift 3
    }
    declare sb tmp
    declare -i i

    sb1=""
    sb1+="$(du -h $_file | cut -d$'\t' -f1)"
    sb1+=" | "
    sb1+="$(identify -format '%wx%h' "$_file[0]")"
    sb1+=" | "
    sb1+="$_file"

    sb2=""
    (( a )) && {
        sb2+="${a}/${max}"
        sb2+=" | "
    }
    (( ${#_f[@]} > 1 )) && {
        sb2+="$((_f_index+1))/${#_f[@]}"
    }

    pad="$(printf "%$((_cols - ${#sb1} - ${#sb2}))s" ' ')"

    sb="$sb1$pad$sb2"

    o+="\e[$((_lines - 1));1;H"
    o+="\e[2K"

    o+="\e[$_lines;1;H"
    o+="\e[2K"

    tmp=""
    for ((i=0;i<$_cols;++i)); do
        tmp+="─"
    done
    o+="\e[$((_lines - 1));1;H$tmp"
    tmp=""

    o+="\e[$_lines;1;H$sb"
}
