#!/usr/bin/env bash

function prev-elem {
    if ((! _f_index <= 0)); then
        ((--_f_index))
    fi
}
