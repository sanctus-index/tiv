#!/usr/bin/env bash

function draw {
    declare w3mimgdisplay="/usr/lib/w3m/w3mimgdisplay"
    declare filename="$_file"
    declare -i max_width max_height
    declare -i width height
    declare -i termh=14

    read width height < <(echo -e "5;$filename" | $w3mimgdisplay)
    read max_width max_height < <($w3mimgdisplay -test)


    if ((width > max_width)); then
        height=$((height * max_width / width))
        width=$max_width
    fi

    if ((height > max_height)); then
        width=$((width * max_height / height))
        height=$max_height
    fi

    declare -i x=$(((max_width - width) / 2))
    declare -i y=$(((max_height - height) / 2))

    height=$((height - (termh * 2)))

    echo -en "0;1;${x};${y};${width};${height};;;;;${filename}\n4;\n3;\n" | $w3mimgdisplay
}

function draw-animation {
    [[ ! -e "$_tmpdir/$_file" ]] && {
        mkdir "$_tmpdir/$_file"
        body="LOADING..."
        echo -en "\e[$(((_lines - 1) / 2));$(((_cols - ${#body}) / 2));H$body"
        ffmpeg -i "$_file" -vsync 0 "$_tmpdir/$_file/%05d.png" 2>&1 &>/dev/null
    }

    declare w3mimgdisplay="/usr/lib/w3m/w3mimgdisplay"
    declare first_frame="$_tmpdir/$_file/00001.png"
    declare -i max_width max_height
    declare -i width height
    declare -i termh=14

    # sxiv $first_frame

    read width height < <(echo -e "5;$first_frame" | $w3mimgdisplay)
    read max_width max_height < <($w3mimgdisplay -test)


    if ((width > max_width)); then
        height=$((height * max_width / width))
        width=$max_width
    fi

    if ((height > max_height)); then
        width=$((width * max_height / height))
        height=$max_height
    fi

    declare -i x=$(((max_width - width) / 2))
    declare -i y=$(((max_height - height) / 2))

    height=$((height - (termh * 2)))

    declare -a frames=( "$_tmpdir/$_file/"*".png" )

    declare -i i=0

    while :; do
        for f in ${frames[@]}; do
            statusbar -a $i ${#frames[@]}

            ((i>=${#frames[@]})) && ((i=1)) || ((++i))

            echo -en "0;1;${x};${y};${width};${height};;;;;${f}\n4;\n3;\n" | $w3mimgdisplay

            o+="\e[H"

            echo -en "$o"
            o=""

            OFS="$IFS"
            IFS=""
            read -rsn1 -t 0.05 _key
            IFS="$OFS"

            [[ -n $_key ]] && return 0
        done
    done

}
