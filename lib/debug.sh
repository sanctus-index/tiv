#!/usr/bin/env bash

function debug {
    declare -p > /tmp/dump.txt
    [[ -z $1 ]] && return
    notify-send "[DEBUG]" "$*"
}
