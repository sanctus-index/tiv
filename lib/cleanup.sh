#!/usr/bin/env bash

function cleanup {
    declare o

    read -rt 0.001 && cat </dev/stdin>/dev/null

    stty echo                   # enable kbd output to terminal
    o+="\033[2J"                # clear screen
    o+="\e[?1049l\e[23;0;0t"    # revert old terminal screen
    o+="\e[?12l\e[?25h"         # unhide the cursor
    o+="${_reset}"

    echo -en "${o}"

    rm -r $_tmpdir

    exit 0
}
