#!/usr/bin/env bash

function loop {
    declare -i _f_index=0

    o=""

    while :; do

        _file="${_f[$_f_index]}"

        if [[ $(is-animation "$_file") ]]; then
            draw-animation "$_file"
        else
            statusbar

            draw "$_file"

            echo -en "$o"
            o=""

            OFS="$IFS"
            IFS=""
            read -rsn1 _key
            IFS="$OFS"
        fi


        [[ -z $_key ]] && _key='\n'
        [[ $_key == $'\u1b' ]] && read -rsn2 _key
        [[ -n $_key ]] && {
            case $_key in
                n|'[C') next-elem ;;
                p|'[D') prev-elem ;;
                '\n') :           ;;
                ' ') read-space   ;;
                q) exit 0         ;;
                *) :              ;;
            esac
        }

        echo -en "\e[2J\e[H"
    done
}
