#!/usr/bin/env bash

function next-elem {
    if ((_f_index+1 < ${#_f[@]})); then
        ((++_f_index))
    fi
}
