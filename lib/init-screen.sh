#!/usr/bin/env bash

function init-screen {
    declare o

    stty -echo                  # disable kbd output to terminal
    o+="\e[?1049h\e[22;0;0t"    # save previous screen
    o+="\e[?25l"                # hide cursor
    o+="\e[H\e[2J\e[3J"         # clear screen

    echo -en "${o}"

    trap 'cleanup' HUP TERM EXIT INT
}
