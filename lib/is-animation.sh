#!/usr/bin/env bash

function is-animation {
    declare file="$1"
    declare ext=${file##*.}

    case ${ext} in
        gif|webm|mkv|mp4) echo -n 1 ;;
        *) : ;;
    esac

}
