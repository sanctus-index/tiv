#!/usr/bin/env bash

function init-config {
    [[ -e $TIV_CONFIG ]] && return 0

    mkdir -p $TIV_CONFIG

    cat > "$TIV_CONFIG/space_command.sh" << 'EOB'
# Script to be called when the space key was invoked.
#
# Please prepend each function with a custom prefix, so there are no
# name collisions.

# Variables:
#  $_file = current file
#  ${_f[@]} = array of loaded files
#  $_f_index = current index of the file

# Add more rules to the case statement, if you want to add custom
# commands.  This is bash, so you can use all of bash in here.  But do
# not modify the `read-name' function name, as it is called when the
# space key is issued.
function read-space {
    read -rsn1 _space_key

    case ${_space_key} in
        n) user/notify-send-file "$_file" ;;
        d) user/delete-file "$_file"      ;;
        *) :                              ;;
    esac
}

function user/notify-send-file {
    notify-send "Terminal Image Viewer" "File: $_file"
}

function user/delete-file {
    rm -r "$_file"
    unset '_f[$_f_index]'
}
EOB

    chmod +x "$TIV_CONFIG/space_command.sh"
}
