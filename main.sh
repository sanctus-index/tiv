#!/usr/bin/env bash

# tiv -- Terminal Image Viewer

# Copyright (C) 2021 Finn Sauer <info@finnsauer.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Commentary:
#
# A terminal image viewer.  This project was just meant to be for
# exploring the possibilities of w3m's image display program.  It
# naturally matured into this; bear that in mind when you find some
# functionality to be sluggish.

### NOTICE ###########################
#                                    #
# CURRENTLY UNDER HEAVY DEVELOPMENT! #
#                                    #
######################################

[[ -z $1 ]] && exit 1

: "${XDG_CONFIG_HOME:=$HOME/.config}"
: "${TIV_CONFIG:=$XDG_CONFIG_HOME/tiv}"

declare -g _this _dir _name

_this="$(readlink -f ${BASH_SOURCE[0]})"
_dir="${_this%/*}"
_name="${_this##*/}"

for f in "$_dir/lib/"*".sh"; do
    . "${f}"
done


function main {

    declare -x o
    declare -i _x _y
    declare -a _f=( "$@" )
    declare _tmpdir=$(mktemp -d)
    read -r _lines _cols < <(stty size)

    init-config

    load-config

    init-screen

    loop
}

main "${@:-}"
