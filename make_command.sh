#!/usr/bin/env bash

declare -g _this _dir _name _output

_this="$(readlink -f ${BASH_SOURCE[0]})"
_dir="${_this%/*}"
_name="${_this##*/}"
_output="${_dir}/$*"

{
awk '{
    if ($0 ~ /^for f in "\$_dir\/lib\/"\*"\.sh"; do$/) {
        exit
    } else {
        print $0
    }
}' "${_dir}/main.sh"

awk '{
    if (!($0 ~ /^#!\/usr\/bin\/env bash$/)) {
        print $0
    }
}' < <(cat "${_dir}/lib/"*".sh")

awk 'BEGIN{
    f=0
}
{
    if ($0 ~ /^for f in "\$_dir\/lib\/"\*"\.sh"; do$/) {
        f=1
    }

    if (f>0&&f<=4) {
        f+=1
    }

    if (f==5) {
        print $0
    }
}' "${_dir}/main.sh"

} > "$_output"

chmod +x "$_output"
